/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:43:31
 * @LastEditors: LiLei
 * @LastEditTime: 2021-10-25 19:52:36
 */
module.exports = {
    plugins: {
        "postcss-px-to-viewport": {
            unitToConvert: "px",
            viewportWidth: 750,
            viewportHeight: 1334,
            propList: ["*"],
            fontViewportUnit: "vw",
            // selectorBlackList: [],
            replace: true,
            // exclude: /(\/|\\)(node_modules)(\/|\\)/,
            unitPrecision: 3,
            viewportUnit: "vw",
            selectorBlackList: [".ignore", ".hairlines"],
            minPixelValue: 1,
            mediaQuery: false,
        },
    },
};

// const result = {
//     unitToConvert: "px",
//     viewportWidth: 750,
//     viewportHeight: 1334,
//     propList: ["*"],
//     fontViewportUnit: "vw",
//     // selectorBlackList: [],
//     replace: true,
//     // exclude: /(\/|\\)(node_modules)(\/|\\)/,
//     unitPrecision: 3,
//     viewportUnit: "vw",
//     selectorBlackList: [".ignore", ".hairlines"],
//     minPixelValue: 1,
//     mediaQuery: false,
// };

// module.exports = (opts = result) => {
//     return {
//         postcssPlugin: "postcss-px-to-viewport",
//         Once(root, { result }) {
//             root.walkAtRules((atRule) => {
//                 return {
//                     unitToConvert: "px",
//                     viewportWidth: 750,
//                     viewportHeight: 1334,
//                     propList: ["*"],
//                     fontViewportUnit: "vw",
//                     // selectorBlackList: [],
//                     replace: true,
//                     // exclude: /(\/|\\)(node_modules)(\/|\\)/,
//                     unitPrecision: 3,
//                     viewportUnit: "vw",
//                     selectorBlackList: [".ignore", ".hairlines"],
//                     minPixelValue: 1,
//                     mediaQuery: false,
//                 }
//             });
//         },
//     };
// };
// module.exports.postcss = true;
