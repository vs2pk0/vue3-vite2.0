/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:01:19
 * @LastEditors: LiLei
 * @LastEditTime: 2021-10-25 17:39:46
 */
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
const { resolve } = require("path");

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    server: {
        // ← ← ← ← ← ←
        host: "0.0.0.0", // ← 新增内容 ←
    },
    productionSourceMap: false, // 生产环境下css 分离文件
    css: {
        // 引用全局 scss
        preprocessorOptions: {
            scss: {
                additionalData: '@import "./src/module.scss";',
            },
        },
    },
    resolve: {
        //别名
        alias: {
            "/@": resolve(__dirname, "./src"), //注意 后面/不需要了
        },
    },
});
