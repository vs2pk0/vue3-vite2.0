/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:17:40
 * @LastEditors: LiLei
 * @LastEditTime: 2021-10-25 17:50:03
 */
import { createStore } from "vuex";

export default createStore({
    state: {},
    mutations: {},
    actions: {},
    modules: {},
});
