/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:01:19
 * @LastEditors: LiLei
 * @LastEditTime: 2021-10-25 17:18:10
 */
import { createApp } from "vue";
import router from "./router";
import store from "./store";
import App from "./App.vue";

createApp(App).use(router).use(store).mount("#app");
