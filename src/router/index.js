/*
 * @Descripttion: Descripttion
 * @Author: LiLei
 * @Date: 2021-10-25 17:17:40
 * @LastEditors: LiLei
 * @LastEditTime: 2021-10-25 17:18:39
 */
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: '/',
    redirect: '/index'
  },
  {
    path: "/index",
    name: "index",
    component: () => import( "../pages/index/index.vue")
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
